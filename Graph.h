#pragma once
#include <iostream>
#include <fstream> 
#include <vector>
#include <map>
#include <list>
#include <string>
#include <algorithm>

using namespace std;

class Graph {
private:
	map<string, list<pair<string, double>>> l_Adj;
	bool orient;
public:
	Graph(const char* filename);
	Graph(bool o) : orient(o) {};
	Graph(const Graph& object);		
	~Graph() {};

	bool getOrient() { return orient; };
	bool does_edge_exist(string from, string to);
	void addVer(string ver);
	void addEdge(string from, string to, double wgh);
	bool removeVer(string ver);
	bool removeEdge(string from, string to);

	void getAdjList();  
	vector<string> getVerList();
	vector<pair<string, string>> getEdgeList();
	map<string, list<pair<string, double>>> get_l_Adj() { return l_Adj; };
	void outLst();
};

Graph::Graph(const char* filename) {
	ifstream in(filename, ios_base::in);
	if (in.is_open()) {
		string str, x, y, wei;
		getline(in, str);
		if (str == "not directed graph")
			orient = false;
		else if (str == "directed graph")
			orient = true;
		int k;
		while (!in.eof()) {
			getline(in, str);
			k = str.find(" ");
			if (k == string::npos && str.length() != 0) {
				x = str;
				addVer(x);
			}
			else if (str.length() != 0) {
				x = str.substr(0, k);
				k = str.find_first_of(" ", k + 1);
				y = str.substr(x.length() + 1, k - x.length() - 1);
				wei = str.substr(x.length() + y.length() + 1);
				for (int i = 0; i < wei.length(); i++)
					if (wei[i] == '.')  wei[i] = ',';
				double w = std::stof(wei);
				addEdge(x, y, w);
			}
		}
		in.close();
	}
}

Graph::Graph(const  Graph& other) {
	this->orient = other.orient;
	this->l_Adj = other.l_Adj;
}

vector<string> Graph::getVerList() {
	vector <string> verList;
	for (auto it = l_Adj.begin(); it != l_Adj.end(); it++)
		verList.push_back(it->first);
	return verList;
}

vector<pair<string, string>> Graph::getEdgeList() {
	vector<pair<string, string>> edgeList;
	for (auto it1 = l_Adj.begin(); it1 != l_Adj.end(); it1++) {
		for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++)
			edgeList.push_back(make_pair(it1->first, it2->first));
	}
	return edgeList;
}

void Graph::getAdjList() {
	for (auto it1 = l_Adj.begin(); it1 != l_Adj.end(); it1++) {
		cout << it1->first << " ";
		for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++)
			cout << "(" << it2->first << ", " << it2->second << ") ";
		cout << endl;
	}
};

bool Graph::does_edge_exist(string from, string to) {
	bool fl = false;
	auto it_x = l_Adj.find(from);
	if (it_x != l_Adj.end()) {
		for (auto it_y = it_x->second.begin(); it_y != it_x->second.end(); it_y++)
			if (it_y->first == to) {
				fl = true;
				break;
			}
	}
	return fl;
}

void Graph::addVer(string ver) {
	l_Adj[ver];
}

void Graph::addEdge(string from, string to, double wgh) {
	auto it_x = l_Adj.find(from);
	bool fl = true;
	if (it_x != l_Adj.end()) {
		for (auto it_y = it_x->second.begin(); it_y != it_x->second.end(); it_y++)
			if (it_y->first == to) {
				fl = false;
				break;
			}
	}
	if (orient) {
		if (fl) { //����� �� ����������
			l_Adj[from].push_back(make_pair(to, wgh));
			if (l_Adj.find(to) == l_Adj.end()) {
				l_Adj[to].push_back(make_pair("0", 0.0));
				auto it = l_Adj.find(to);
				it->second.erase(it->second.begin());
			}
		}
	}
	else if (fl) { //������ ��� �� �������
		l_Adj[from].push_back(make_pair(to, wgh));
		l_Adj[to].push_back(make_pair(from, wgh));
	}
}

bool Graph::removeEdge(string from, string to) {
	if (does_edge_exist(from, to)) {
		for (auto it = l_Adj[from].begin(); it != l_Adj[from].end(); it++)
			if (it->first == to) {
				l_Adj[from].erase(it);
				break;
			}
		if (!orient)
			removeEdge(to, from);
		return true;
	}
	else return false;
}

bool Graph::removeVer(string ver) {
	auto it = l_Adj.find(ver);
	if (it != l_Adj.end()) {
		for (auto it1 = l_Adj.begin(); it1 != l_Adj.end(); it1++)
			removeEdge(it1->first, ver);
		l_Adj.erase(ver);
		return true;
	}
	else return false;
}

void Graph::outLst() {
	std::ofstream out;
	out.open("output.txt");
	if (out.is_open()) {
		if (orient)
			out << "directed graph\n";
		else
			out << "not directed graph\n";

		for (auto it1 = l_Adj.begin(); it1 != l_Adj.end(); it1++) {
			bool fl = false;
			for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {
				fl = true;
				out << it1->first << " " << it2->first << " " << it2->second << "\n";
			}
			if (!fl)
				out << it1->first << "\n";
		}
	}
}