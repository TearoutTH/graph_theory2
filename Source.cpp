#include "Graph.h"
#include <queue>

using namespace std;

void dfs(map<string, bool> &used, string v, vector<string> vers, vector<pair<string, string>> edges) {
	used[v] = true;
	for (auto it = edges.begin(); it != edges.end(); it++) {
		if (it->first == v && used[it->second] != true) {
			dfs(used, it->second, vers, edges);
		}
	}
}

bool bfs(string v, vector<string> vers, vector<pair<string, string>> edges) {
	queue <string> Queue;
	map<string, bool> used;
	map<string, int> way;
	vector <string> buf;
	string new_v = v;
	bool fl = true;
	bool bl = false;
	for (auto it = vers.begin(); it != vers.end(); it++)
		used[*it] = false;
	for (auto it = vers.begin(); it != vers.end(); it++)
		way[*it] = 0;
	for (auto it = edges.begin(); it != edges.end(); it++) {
		if (it->first == v) {
			Queue.push(it->second);
			way[it->second] ++;
		}
	}
	used[v] = true;
	while (!Queue.empty()) {
		new_v = Queue.front();
		used[new_v] = true;
		Queue.pop();
		for (auto it = edges.begin(); it != edges.end(); it++) {
			if (it->first == new_v && used[it->second] != true) {
				Queue.push(it->second);
				if (way[it->second] == 0)
					way[it->second] = way[new_v] + 1;
			}
		}
	}
	/*cout << endl << "�������: \n";
	for (auto it = vers.begin(); it != vers.end(); it++) {
		cout << *it << " ";
	}
	cout << endl << "����: \n";
	for (auto it = way.begin(); it != way.end(); it++) {
		cout << it->second << " " << endl;
	}*/
	if (way[*vers.begin()] == 0)
		way[*vers.begin()] = way[*(vers.begin() + 1)];
	for (auto it = vers.begin(); it != vers.end(); it++)
		if (way[*it] != 0)
			bl = true;

	if (bl) {
		for (auto it = vers.begin(); it != vers.end(); it++)
			if (*it != v) {
				if (way[*it] != way[*vers.begin()])
					fl = false;
			}
		return fl;
	}
	else
		return bl;
}

int main() {

	setlocale(LC_ALL, "rus");

	int a = -1;
	int b = -1;
	int c = -1;
	const char* name;
	string name1;
	name = name1.c_str();
	string from;
	string to;
	double weight;
	bool fl;
	vector<string> vers;
	vector<pair<string, string>> edges;
	map<string, list<pair<string, double>>> l_adj;
	vector <string> buf;
	vector <string> smegn;
	map<string, bool> used;
	cout << "� ����� ������ ������ ��������?\n";
	cin >> name1;
	name = name1.c_str();
	Graph g = Graph(name);
	Graph g2 = Graph(name);
	while (a != 0) {
		cout << "��� �� �� ������ �������?\n 1 - ������������� ����\n 2 - ���������� ������ ��������� �����\n 3 - ��������� ���� �� �������\n 4 - ������� ����\n 0 - ����� �� ���������\n";
		cin >> a;
		cout << endl;
		switch (a) {
		case 1:
			while (b != 0) {
				cout << "�������� ��������:\n 1 - ������� �������\n 2 - �������� �������\n 3 - ������� �����(����)\n 4 - �������� �����(����)\n 0 - ����� �� ������ ��������������\n";
				cin >> b;
				cout << endl;
				switch (b) {
				case 1: {
					cout << "����� ������� ���������� �������?\n Ver = ";
					cin >> from;
					cout << endl;
					if (g.removeVer(from))
						cout << "���������!\n";
					else
						cout << "�� ����������, ������ ����� ������� ��� � �����\n";
					break;
				}
				case 2:
					cout << "����� ������� ���������� ��������?\n Ver = ";
					cin >> from;
					cout << endl;
					vers = g.getVerList();
					vers.push_back("End_ver");
					g.addVer(from);
					break;
				case 3:
					cout << "����� �����(����) ���������� �������?\n From = ";
					cin >> from;
					cout << endl;
					cout << "To = ";
					cin >> to;
					cout << endl;
					if (g.removeEdge(from, to))
						cout << "���������!\n";
					else
						cout << "�� ����������, ������ ������ ����� ��� � �����\n";
					break;
				case 4:
					cout << "����� �����(����) ���������� ��������?\n From = ";
					cin >> from;
					cout << endl;
					cout << " To = ";
					cin >> to;
					cout << endl;
					cout << " Weight = ";
					cin >> weight;
					cout << endl;
					edges = g.getEdgeList();
					if (find(edges.begin(), edges.end(), make_pair(from, to)) != edges.end())
						cout << "����� ����� ����� ��������� ��� ���� � �����, ���� �� ������ ��� ��������, �� ������� � �������� ��� ������, ����������!\n";
					else {
						g.addEdge(from, to, weight);
						cout << "���������!\n";
					}
					break;
				default:
					break;
				}
			}
			b = -1;
			break;
		case 2:
			cout << "������ ��������� �����:\n";
			g.getAdjList();
			cout << endl;
			break;
		case 3:
			while (c != 0) {
				cout << "�������� �������:\n 1 - la1 - ����� ������ ������� � ������\n 2 - la2 - ����� \"���������\" ����\n 3 - ��������� ������\n";
				cout << " 4 - ����� ���������� �� ������ ������� ������� ������, �� ������� ����� ������� ������� � ������ �������\n";
				cout << " 5 - ����� �������, ����� ���������� (�� ����� ����) ����� �� ������� �� ���� ��������� ���������\n";
				cout << " 0 - ����� �� ������ ������� �����\n";
				cin >> c;
				cout << endl;
				switch (c) {
				case 1: 
					edges = g.getEdgeList();
					cout << "������� �������\n Ver = ";
					cin >> from;
					for (auto iter = edges.begin(); iter != edges.end(); iter++) {
						buf.push_back("Last");
						if (iter->first == from && iter->second != from && (find(buf.begin(), buf.end(), iter->second) == buf.end())) {
							buf.pop_back();
							buf.push_back(iter->second);
						}
						else  if (iter->second == from && iter->first != from && (find(buf.begin(), buf.end(), iter->first) == buf.end())) {
							buf.pop_back();
							buf.push_back(iter->first);
						}
						else buf.pop_back();
					}
					if (!buf.empty()) {
						cout << "������� �������:\n";
						for (auto it = buf.begin(); it != buf.end(); it++)
							cout << *it << " ";
						cout << endl;
					}
					else
						cout << "���� �������� ���� ������� �� ����������, � ��� ��� ������� ����\n";
					buf.clear();
					break;
				case 2:
					l_adj = g.get_l_Adj();
					cout << "������� �������\n Ver = ";
					cin >> from;
					if (l_adj.find(from) != l_adj.end()) {
						for (auto it2 = l_adj.find(from)->second.begin(); it2 != l_adj.find(from)->second.end(); it2++) {
							buf.push_back(it2->first);
						}
						cout << endl;
					}
					if (!buf.empty()) {
						cout << "�������, ��������������� �������:\n";
						for (auto it = buf.begin(); it != buf.end(); it++)
							cout << *it << " ";
						cout << endl;
					}
					else
						cout << "���� �������� ���� ������� �� ����������, ���� ��� ������, ��������������� ������� ������\n";
					buf.clear();
					break;
				case 3:
					cout << "� ����� ������ ������ ��������?\n";
					cin >> name1;
					name = name1.c_str();
					g2 = Graph(name);
					fl = true;
					if (g.getOrient() == g2.getOrient()) {
						vector<string> lv1 = g.getVerList();
						vector<string> lv2 = g2.getVerList();
						if (lv1 == lv2) {
							map<string, list<pair<string, double>>> l_Adj1 = g.get_l_Adj();
							map<string, list<pair<string, double>>> l_Adj2 = g2.get_l_Adj();
							for (auto it = lv1.begin(); it != lv1.end(); it++) {
								list<pair<string, double>> le1 = l_Adj1[*it];
								list<pair<string, double>> le2 = l_Adj2[*it];
								le2.push_back(make_pair("Last", 0));
								for (auto it1 = le1.begin(); it1 != le1.end(); it1++) {
									if (find(le2.begin(), le2.end(), *it1) == le2.end())
										fl = false;
								}
								le2.pop_back();
								le1.push_back(make_pair("Last", 0));
								for (auto it1 = le2.begin(); it1 != le2.end(); it1++) {
									if (find(le1.begin(), le1.end(), *it1) == le1.end())
										fl = false;
								}
								le1.pop_back();
							}
						}
						else fl = false;
					}
					else fl = false;
					if (fl)
						cout << "����� ��������� ";
					else
						cout << "����� �� ��������� ";
					cout << endl;
					break;
				case 4:
					vers = g.getVerList();
					for (auto it = vers.begin(); it != vers.end(); it++)
						used[*it] = false;
					cout << "������� ������� \n Ver = ";
					cin >> from;
					dfs(used, from, g.getVerList(), g.getEdgeList());
					for (auto it = vers.begin(); it != vers.end(); it++)
						if (used[*it] == true && *it != from)
							smegn.push_back(*it);
					for (auto it = smegn.begin(); it != smegn.end(); it++) {
						for (auto it = vers.begin(); it != vers.end(); it++)
							used[*it] = false;
						dfs(used, *it, g.getVerList(), g.getEdgeList());
						if (used[from] == true) {
							buf.push_back(*it);
						}
					}
					smegn.clear();
					if (!buf.empty()) {
						cout << "�������, ��������������� �������:\n";
						for (auto it = buf.begin(); it != buf.end(); it++)
							cout << *it << " ";
						cout << endl;
					}
					else
						cout << "���� �������� ���� ������� �� ����������, ���� ��� ������, ��������������� ������� ������\n";
					buf.clear();
					break;
				case 5:				
					vers = g.getVerList();
					edges = g.getEdgeList();
					for (auto it = vers.begin(); it != vers.end(); it++)
						if (bfs(*it, vers, edges)) {
							buf.push_back(*it);
							break;
						}
					if (!buf.empty()) {
						cout << "�������, ��������������� �������:\n";
						for (auto it = buf.begin(); it != buf.end(); it++)
							cout << *it << " ";
						cout << endl;
					}
					else
						cout << "����� ������ ���\n";
					buf.clear();
					break;
				default:
					break;
				}
			}
			c = -1;
			break;
		default:
			break;
		}
	}
	/*while (i != 0) {
		cout << "����� �������� ���������� ���������?\n 1 - ������� �������\n 2 - �������� �������\n 3 - ������� �����(����)\n 4 - �������� �����(����)\n 5 - ����������� ������ ��������� �����\n ";
		cout << "6 - la1 - ����� ������� ����\n 7 - la2 - ����� \"���������\" ����\n 8 - ��������� ������\n ";
		cout << "9 - ����� �����\n 0 - ����� �� ���������\n ";
		cin >> i;
		cout << endl;
		switch (i) {
		case 1: {
			cout << "����� ������� ���������� �������?\n Ver = ";
			cin >> from;
			cout << endl;
			if (g.removeVer(from))
				cout << "���������!\n";
			else
				cout << "�� ���������� :�\n";
			break;
		}
		case 2: {
			cout << "����� ������� ���������� ��������?\n Ver = ";
			cin >> from;
			cout << endl;
			g.addVer(from);
			cout << "���������!\n";
			break;
		}
		case 3: {
			cout << "����� �����(����) ���������� �������?\n From = ";
			cin >> from;
			cout << endl;
			cout << "To = ";
			cin >> to;
			cout << endl;
			if (g.removeEdge(from, to))
				cout << "���������!\n";
			else
				cout << "�� ���������� :�\n";
			break;
		}
		case 4: {
			cout << "����� �����(����) ���������� ��������?\n From = ";
			cin >> from;
			cout << endl;
			cout << "To = ";
			cin >> to;
			cout << endl;
			cout << "Weight = ";
			cin >> weight;
			cout << endl;
			g.addEdge(from, to, weight);
			cout << "���������!\n";
			break;
		}
		case 5: {
			g.getAdjList();
			cout << endl;
			break;
		}
		case 6: {
			vector<pair<string, string>> edges = g.getEdgeList();

			string ver;

			cout << "������� �������\n Ver = ";

			cin >> ver;
			fl = false;
			for (auto iter = edges.begin(); iter != edges.end(); iter++) {
				if (iter->first == ver) {
					cout << iter->second << " ";
					fl = true;
				}
				else  if (iter->second == ver) {
					cout << iter->first << " ";
					fl = true;
				}
			}
			if (fl)
				cout << "��� ������� ���� ���� �������" << endl;
			else
				cout << "���� ��� ����� �������, ���� � �� ��� ������� ���� " << endl;
			break;
		}
		case 7: {
			
			map<string, list<pair<string, double>>> l_adj = g.get_l_Adj();

			string ver;

			cout << "������� �������\n Ver = ";

			cin >> ver;
			fl = false;
			auto it = l_adj.find(ver);
			if (it != l_adj.end()) {
				for (auto it2 = it->second.begin(); it2 != it->second.end(); it2++) {
					cout << it2->first << " ";
					fl = true;
				}
				cout << endl;
			}

			if (fl)
				cout << "��� \"���������\" ���� ���� �������" << endl;
			else
				cout << "���� ��� ����� �������, ���� � �� ��� ��������� ���� " << endl;
			break;
		}
		case 8: {
			cout << "� ����� ������ ������ ��������?\n";
			cin >> name1;
			name = name1.c_str();
			Graph g2 = Graph(name);
			fl = true;
			if (g.getOrient() == g2.getOrient()) {
				vector<string> lv1 = g.getVerList();
				vector<string> lv2 = g2.getVerList();
				if (lv1 == lv2) {
					vector<pair<string, string>> le1 = g.getEdgeList();
					vector<pair<string, string>> le2 = g2.getEdgeList();
					le2.push_back(make_pair("Last", "Last"));
					for (auto it1 = le1.begin(); it1 != le1.end(); it1++) {
						if (find(le2.begin(), le2.end(), *it1) == le2.end())
							fl = false;
					}
					le2.pop_back();
					le1.push_back(make_pair("Last", "Last"));
					for (auto it1 = le2.begin(); it1 != le2.end(); it1++) {
						if (find(le1.begin(), le1.end(), *it1) == le1.end())
							fl = false;
					}
					le1.pop_back();
				}
				else fl = false;
			}
			else fl = false;
			if (fl)
				cout << "����� ��������� ";
			else
				cout << "����� �� ��������� ";
			cout << endl;
			break;
		}
		case 10: {
			vector <string> buf;
			vector <string> smegn;
			map<string, bool> used;
			vector<string> vers = g.getVerList();
			for (auto it = vers.begin(); it != vers.end(); it++)
				used[*it] = false;
			string ver;
			cout << "������� ������� \n Ver = ";
			cin >> ver;
			dfs(used, ver, g.getVerList(), g.getEdgeList());
			for (auto it = vers.begin(); it != vers.end(); it++)
				if (used[*it] == true && *it != ver)
					smegn.push_back(*it);
			for (auto it = smegn.begin(); it != smegn.end(); it++) {
				for (auto it = vers.begin(); it != vers.end(); it++)
					used[*it] = false;
				dfs(used, *it, g.getVerList(), g.getEdgeList());
				if (used[ver] == true) {
					buf.push_back(*it);
				}
			}
			if (!buf.empty()) {
				cout << "�������, ��������������� �������:\n";
				for (auto it = buf.begin(); it != buf.end(); it++)
					cout << *it << " ";
				cout << endl;
			}
			else 
				cout << "���� �������� ���� ������� �� ����������, ���� ��� ������, ��������������� ������� ������\n";
			break;
		}
		case 11: {

			break;
		}
		case 9: {
			cout << "� ����� ������ ������ ��������?\n";
			cin >> name1;
			name = name1.c_str();
			g = Graph(name);
			break;
		}
		default:
			break;
		}
	}*/
}
